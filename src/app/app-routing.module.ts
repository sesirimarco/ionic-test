import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {path:'', loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)},
  { path: 'people-details', loadChildren: './pages/people-details/people-details.module#PeopleDetailsPageModule' },
  { path: 'planets-details', loadChildren: './pages/planets-details/planets-details.module#PlanetsDetailsPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

