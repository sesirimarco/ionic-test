import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  API = 'https://swapi.co/api/';
  FIMLS = 'films/';
  PEOPLE = 'people/';
  PLANETS = 'planets/';
  constructor(private http: HttpClient) { }

  getFilms() {
      return this.http.get(this.API + this.FIMLS);
  }

  getFilm(id) {
    return this.http.get(this.API + this.FIMLS + id);
  }
  getPeople() {
      return this.http.get(this.API + this.PEOPLE);
  }

  getCharacter(id) {
    return this.http.get(this.API + this.PEOPLE + id);
  }
  getPlanets() {
      return this.http.get(this.API + this.PLANETS);
  }

  getPlanet(id) {
    return this.http.get(this.API + this.PLANETS + id);
  }
}
