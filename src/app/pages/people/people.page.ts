import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.page.html',
  styleUrls: ['./people.page.scss'],
})
export class PeoplePage implements OnInit {
  people: Observable<any>;
  constructor(private navController: NavController, private router: Router, private api: ApiService) { }

  ngOnInit() {
    this.people = this.api.getPeople();
    console.log(this.people)
  }
  
  openDetail(character){
    let characterId =  character.url.split('people')[1].split('/')[1];
    this.router.navigateByUrl(`/tabs/people/${characterId}`);
  }
}
