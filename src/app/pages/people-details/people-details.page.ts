import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-people-details',
  templateUrl: './people-details.page.html',
  styleUrls: ['./people-details.page.scss'],
})
export class PeopleDetailsPage implements OnInit {

  constructor(private api: ApiService, private activatedRoute: ActivatedRoute) { }
  character: any;
  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id')
    this.api.getCharacter(id).subscribe(res => {
      this.character = res;
      console.log(this.character)
    })
  }

}
