import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.page.html',
  styleUrls: ['./planets.page.scss'],
})
export class PlanetsPage implements OnInit {
  planets: Observable<any>;
  constructor(private navController: NavController, private router: Router, private api: ApiService) { }

  ngOnInit() {
    this.planets = this.api.getPlanets();
  }

  openDetails(planet) {
    let planetId =  planet.url.split('planet')[1].split('/')[1];
    console.log(`planetId: ${planetId}`)
    this.router.navigateByUrl(`/tabs/planets/${planetId}`);
  }

}
